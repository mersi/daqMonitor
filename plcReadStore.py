#!/usr/bin/env python3

import mysql.connector
from configparser import ConfigParser
import time
import snap7
import os
import sys
import logging
import socket

myDir = os.path.dirname(os.path.realpath(__file__))
configFileName = myDir+'/config.ini'

# Check if the configuration file exists
if not os.path.exists(configFileName) :
    print("Config file '%s' does not exist" % configFileName)
    sys.exit(os.EX_CONFIG)

timeStamp = int(time.time())

# read configuration file 
config = ConfigParser()
config.read(configFileName)

# Get the vareialbes from the config file
# Database write access
dbHost = config['mysql']['host']
dbPort = config['mysql']['port']
dbDatabase = config['mysql']['database']
dbUser = config['mysql']['user']
dbPassword = config['mysql']['password']
# PLC read access
plcHostName = config['plc']['node']
plcRack = int(config['plc']['rack'])
plcSlot = int(config['plc']['slot'])

def open_db():
    try:
        mydb = mysql.connector.connect(
            host=dbHost,
            port=dbPort,
            user=dbUser,
            password=dbPassword,
            database=dbDatabase
        )
        logging.info('Connected to database')
    except mysql.connector.Error as err:
        logging.error('Could not connect to database: {}'.format(err))
        mydb = None
    return mydb

def connectPLC(client):
    plcNode = socket.gethostbyname(plcHostName)
    client.connect(plcNode, plcRack, plcSlot)
    if(client.get_connected()):
        return True
    else:
        return False

def readSensor(client, offset):
    sensorDB = 500
    valueSize = 4
    PLCdata = client.db_read(sensorDB, offset, valueSize)
    sensor = snap7.util.get_real(PLCdata, 0)
    return sensor

def storeSensor(mydb, measurement_type_id, myvalue):
    mycursor = mydb.cursor()
    value ="{:.2f}".format(myvalue)
    args = (str(measurement_type_id), str(myvalue), str(timeStamp))
    try:
        mycursor.execute('INSERT INTO measurements (measurement_type_id, value, epoch_timestamp) VALUES (%s, %s, %s);', args)
        mydb.commit()
        #print(timestamp, myvalue)
        logging.info('Data stored successfully')
        #print(logging.info)
    except mysql.connector.Error as err:
        logging.error('Error storing data in database: {}'.format(err))
        print('Error storing data in database: {}'.format(err))

def mainSensor():
    myClient = snap7.client.Client()
    if (connectPLC(myClient)):
        offset_list = [22, 26, 30, 34, 38, 42, 46, 50, 54, 58, 62, 66, 70, 74, 78, 82, 2, 6, 10, 14, 18]
        measurement_type_id_list = list(range(1, 22))
        mydb = open_db()
        if (mydb != None):
            for i in range(len(offset_list)):
                sensor = readSensor(myClient, offset_list[i])
                measurement_type_id = measurement_type_id_list[i]
                storeSensor(mydb, measurement_type_id, sensor)
        else:
            print("Failed to open database for store.")
        mydb.close()

mainSensor()







