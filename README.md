# Installation

In order to install you run this first
```bash
pip3 install --user python-snap7
pip3 install --user mysql-connector
```

In order to install in crontab, you should add this line to execute it every minute
```
* * * * * /install_dir/plcReadStore.py
```

